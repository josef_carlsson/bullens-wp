var end = new Date('03/08/2017 11:50 PM');

    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;
    // var video = $('video');

    function showRemaining() {

        var now = new Date();
        var distance = end - now;

        if (distance < 0) {
          clearInterval(timer);
          document.querySelector('.countdown').innerHTML = '00:00:00';

          //Flicker effect on counter end
          $('.wrapper').effect('pulsate', 'slow');

          //Fit video to window
          $(".video-container").fitVids();
          //Play video
          setTimeout(function() {
            //playVideo();
          }, 1500);
          return;
        }

        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);
        var countdown = document.getElementById('countdown');

        if(hours < 10) {
          countdown.innerHTML = '0' + hours + ':';
        } else {
          countdown.innerHTML = hours + ':';
        }
        if(minutes < 10) {
          countdown.innerHTML += '0' + minutes + ':';
        } else {
          countdown.innerHTML += minutes + ':';
        }

        if(seconds < 10) {
          countdown.innerHTML += '0' + seconds;
        } else {
          countdown.innerHTML += seconds;
        }


    }

    function playVideo() {
      $('video').fadeIn(500);
      document.getElementById('video').play();
    }

    timer = setInterval(showRemaining, 1000);
