<?php

	class Database {

		/** @var PDO */
		public static $connection = FALSE;

		public static function connect( $host, $user, $password, $database ) {
			$dbn = 'mysql:dbname=' . $database . ';host=' . $host;
			try {
				Database::$connection = new PDO( $dbn, $user, $password, array(
					PDO::MYSQL_ATTR_LOCAL_INFILE => true
				) );

				Database::$connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				Database::$connection->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

			} catch( PDOException $e ) {
				echo 'Connection failed: ' . $e->getMessage();
			}
		}

	}

?>